const myName = 'Lisa';
const myAge = 12;
const sum = (a: number, b: number) => {
  return a + b;
};
sum(12, 20);

class Persona {
  // private age;
  // public name;

  constructor(
    private age: number,
    private name: string,
  ) {}

  getSummary() {
    return `My name is ${this.name}, I'm ${this.age} years old`;
  }
}

const lisa = new Persona(12, 'Lisa');
lisa.getSummary();

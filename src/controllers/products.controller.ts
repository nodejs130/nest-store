import {
  Controller,
  Get,
  Param,
  Query,
  Post,
  Body,
  Put,
  Delete,
  HttpStatus,
  HttpCode,
  ParseIntPipe as ParseIntPipeNest,
} from '@nestjs/common';
import { ParseIntPipe as ParseIntPipeCustom } from 'src/common/parse-int.pipe';
import { CreateProductDto, UpdateProductDto } from 'src/dtos/products.dto';
import { ProductsService } from 'src/services/products.service';

@Controller('products')
export class ProductsController {
  constructor(private productService: ProductsService) {}

  @Get()
  getProducts(
    @Query('limit') limit: number = 100,
    @Query('offset') offset: number = 0,
    @Query('brand') brand: string,
  ) {
    // return {
    //   message: `Products: limit ${limit} - offset ${offset} - brand ${brand}`,
    // };
    return this.productService.findAll();
  }

  @Get('filter')
  getProductFilter() {
    return { message: `Products Filter` };
  }

  @Get(':productId')
  @HttpCode(HttpStatus.ACCEPTED)
  getProduct(@Param('productId', ParseIntPipeCustom) productId: number) {
    // return { message: `Product ${productId}` };
    return this.productService.findOne(productId);
  }

  @Post()
  create(@Body() payload: CreateProductDto) {
    // return {
    //   message: 'accion de crear',
    //   payload,
    // };

    return this.productService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipeNest) id: number,
    @Body() payload: UpdateProductDto,
  ) {
    // return {
    //   message: 'accion de modificar',
    //   id,
    //   payload,
    // };
    return this.productService.update(id, payload);
  }

  @Delete(':id')
  delete(@Param('id', ParseIntPipeNest) id: number) {
    return this.productService.remove(id);
  }
}
